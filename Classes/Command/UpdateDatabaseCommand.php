<?php


namespace DKM\AccessControlSync\Command;

use DKM\AccessControlSync\Service\DatabaseUpdateService;
use DKM\AccessControlSync\Utility;
use Dotenv\Dotenv;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\FormatterHelper;
use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ChoiceQuestion;
use Symfony\Component\Console\Question\ConfirmationQuestion;
use TYPO3\CMS\Core\Core\Environment;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Utility\PathUtility;

class UpdateDatabaseCommand extends Command
{
    protected function configure()
    {
        $this->setDescription('Updates access control records (be_groups) from defined sync file')
            ->setName('access_control_sync:database:update')
            ->addArgument(
                'force',
                InputArgument::OPTIONAL,
                'Force update all sync data to Database',
                0
            )
            ->addArgument(
                'matchingField',
                InputArgument::OPTIONAL,
                'Field to match on when updating.',
                'tx_access_control_sync_uuid'
            )
            ->addArgument(
                'no-interaction',
                InputArgument::OPTIONAL,
                'Do not ask any interactive questions. Default value is chosen.',
                false
            );
        parent::configure();
    }

    /**
     * @return int
     * @throws \TYPO3\CMS\Core\Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /** @var DatabaseUpdateService $databaseUpdateService */
        $databaseUpdateService = GeneralUtility::makeInstance(DatabaseUpdateService::class);

        $dotEnv = Dotenv::createImmutable(Environment::getProjectPath());
        $dotEnv->safeLoad();
        if(in_array(Utility::getExtensionConfiguration('mode'), ['Production/Live', 'Production/Staging']) && !file_exists(Environment::getProjectPath() . "/.env")) {
            $errorMessages = ['',' ERROR: No .env file was found in project path ',''];
            $formatter = new FormatterHelper();
            $formattedBlock = $formatter->formatBlock($errorMessages, 'error');
            $output->writeln($formattedBlock);
            $output->writeln('When mode is set Production/Live to Production/Staging, the TYPO3_CONTEXT should be set in a .env file in the project path.');
            $output->writeln('For example TYPO3_CONTEXT="Production/Live"');
            $output->writeln('No update was performed.');
            return 1;
        }

        $runUpdate = true;
        $forceUpdate = $input->getArgument('force');
        $newSyncFile = $databaseUpdateService->isSyncFileUpdated();
        $syncMode = Utility::getMode();

        $output->writeln('### Access Control Sync ###');
        $output->writeln('Sync file: ' . rtrim(PathUtility::getRelativePath(Environment::getProjectPath(), Utility::getSyncStoredFileName()), '/'));

        $switchValue = $syncMode . ($forceUpdate ? ':force' : '') . ($newSyncFile ? ':newSyncFile' : '');
        $output->writeln('Installation Sync Mode: ' . $switchValue);
        $defaultValue = false;
        switch ($switchValue) {
            case 'replica':
                $output->writeln('No new sync data, nothing performed.');
                break;
            case 'primary:force':
            case 'primary:force:newSyncFile':
            case 'replica:force':
                $defaultValue = false;
                $question = new ConfirmationQuestion('Are you sure that you want to force update all sync data to the Database? [No]', $defaultValue);
                $responseText = ['decline' => 'No force sync performed now.', 'accept' => 'Performing force update.'];
                break;
            case 'primary':
            case 'primary:newSyncFile':
                $defaultValue = false;
                $question = new ConfirmationQuestion('Installation is in primary mode, do you want to force update all sync data to Database? [No]', $defaultValue);
                $responseText = ['decline' => 'No force update performed now.', 'accept' => 'Performing force update!'];
                break;
            case 'replica:newSyncFile':
                $defaultValue = true;
                $question = new ConfirmationQuestion('New sync data is available, do you want to perform the sync update now? [Yes]', $defaultValue);
                $responseText = ['decline' => 'No update performed now.', 'accept' => 'Performing update!'];
                break;
            case 'replica:force:newSyncFile':
                $defaultValue = 0;
                $question = new ChoiceQuestion('New sync data is available, are you sure that you want to force update all sync data?', ['Do not force update','Force update','Sync updated records only'], $defaultValue);
                $responseText = ['decline' => 'No update performed now.', 'accept' => 'Performing force update!', 's' => 'Performing update on updated records only.' ];
        }
        if(isset($question)) {
            if($input->getArgument('no-interaction')) {
                $result = $defaultValue;
            } else {
                $helper = $this->getHelper('question');
                $result = $helper->ask($input, $output, $question);
                if($switchValue === 'replica:force:newSyncFile') {
                    $qArr = array_flip($question->getChoices());
                    $result = $qArr[$result];
                }
            }
            switch (true) {
                case $result === 1:
                case $result === true :
//                    $output->writeln('result1:' . $result);
                    if ($switchValue === 'primary:newSyncFile' || $switchValue === 'primary') {
                        $forceUpdate = true;
                    }
                    $output->writeln($responseText['accept'] ?? 'Error - message missing');
                    break;
                case $result === false :
                case $result === 0:
//                    $output->writeln('result2:' . $result);
                    $runUpdate = false;
                    $output->writeln($responseText['decline'] ?? 'Error - message missing');
                    break;
                case $result === 2:
//                    $output->writeln('result3:' . $result);
                    $forceUpdate = false;
                    $output->writeln($responseText['s'] ?? 'Error - message missing');
                    break;
            }
//            $output->writeln('$forceUpdate:' . $forceUpdate);
            if($runUpdate) {
                $databaseUpdateService->setMessageMethod(DatabaseUpdateService::MESSAGE_RETURN_DATA);
                $messages = $databaseUpdateService->process($input->getArgument('matchingField'), $forceUpdate);
                if($messages) {
                    $table = new Table($output);
                    $table
                        ->setHeaders(['Severity', 'Title', 'Uuid', 'Message'])
                        ->setRows($messages)
                        ->render();
                } else {
                    $output->writeln('EXT:access_control_sync - There were no new data to sync.');
                }
            }
        }
        return 0;
    }
}