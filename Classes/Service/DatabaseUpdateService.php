<?php


namespace DKM\AccessControlSync\Service;


use DKM\AccessControlSync\Utility;
use TYPO3\CMS\Core\Core\Bootstrap;
use TYPO3\CMS\Core\Core\Environment;
use TYPO3\CMS\Core\Database\Connection;
use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Database\Query\QueryBuilder;
use TYPO3\CMS\Core\DataHandling\DataHandler;
use TYPO3\CMS\Core\Messaging\FlashMessage;
use TYPO3\CMS\Core\Service\OpcodeCacheService;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * Class DatabaseUpdateService
 * @package DKM\AccessControlSync\Service
 */
class DatabaseUpdateService
{

    static $fieldNameUpdateFilter = [
        'uid',
        'pid',
//        'tstamp',
        'crdate',
        'cruser_id',
    ];

    const MESSAGE_RETURN_DATA = 'returnData';
    const MESSAGE_FLASH = 'flashMessage';

    protected $messages = [];

    protected $messageMethod = self::MESSAGE_RETURN_DATA;

    /**
     * @var string Path to additional local file, relative to the public web folder
     */
    protected $additionalConfigurationFile = 'AdditionalConfiguration.php';

    /**
     * Get the file location of the additional configuration file,
     * currently the path and filename.
     *
     * @return string
     * @internal
     */
    public function getAdditionalConfigurationFileLocation()
    {
        return Environment::getLegacyConfigPath() . '/' . $this->additionalConfigurationFile;
    }

    /**
     * @param $message
     * @param $title
     * @param $severity
     * @param string $uuid
     */
    protected function addMessage($message, $title, $severity, $uuid = '') {
        switch ($this->messageMethod) {
            case self::MESSAGE_RETURN_DATA:
                $this->messages[] = [
                    'severity' => Utility::getSeverity($severity),
                    'title' => Utility::getLanguageService()->sL($title),
                    'uuid' => $uuid,
                    'message' => Utility::getLanguageService()->sL($message)
                ];
                break;
            case self::MESSAGE_FLASH:
                Utility::flashMessage($message, $title . ($uuid ? " (uuid: {$uuid})": ''), $severity);
                break;
        }
    }

    /**
     * @param $method
     */
    public function setMessageMethod($method) {
        switch ($method) {
            case self::MESSAGE_RETURN_DATA:
            case self::MESSAGE_FLASH:
                $this->messageMethod = $method;
                break;
        }
    }

    /**
     * @return bool
     * @throws \TYPO3\CMS\Core\Exception
     */
    public function isSyncFileUpdated() {
        $extCfg = Utility::getExtensionConfiguration();
        $absSyncFilePath = Utility::getSyncStoredFileName();
        $modificationTime = Utility::getTimeFromSyncFileName($absSyncFilePath);
        return file_exists($absSyncFilePath) && $modificationTime > $extCfg['lastSync_fileModificationTime'];
    }

    /**
     * @param string $identifierField
     * @param false $forceUpdate
     * @return mixed[]|null
     * @throws \TYPO3\CMS\Core\Exception
     */
    public function process($identifierField = 'tx_access_control_sync_uuid', $forceUpdate = false) {
        if(!$forceUpdate && Utility::getMode() != 'replica') {
            $this->addMessage('Installation not in replica mode', 'WARNING', \TYPO3\CMS\Core\Type\ContextualFeedbackSeverity::NOTICE);
            return $this->messages;
        }
        $extCfg = Utility::getExtensionConfiguration();
        $absSyncFilePath = Utility::getSyncStoredFileName();

        $opcodeCacheService = GeneralUtility::makeInstance(OpcodeCacheService::class);
        $opcodeCacheService->clearAllActive($absSyncFilePath);
        $opcodeCacheService->clearAllActive($this->getAdditionalConfigurationFileLocation());
        $modificationTime = Utility::getTimeFromSyncFileName($absSyncFilePath);
        if(file_exists($absSyncFilePath) && $modificationTime > ($forceUpdate ? 0 :$extCfg['lastSync_fileModificationTime'])) {
            $configuration = require $absSyncFilePath;

            if($this->updateBackendGroups($configuration, $forceUpdate ? 0 : $extCfg['lastSync_fileModificationTime'], $identifierField)) {
                Utility::setLastSync_fileModificationTime($modificationTime);
            } else {
                $this->addMessage('Solve the problem, and relogin to try again','Some updates failed', \TYPO3\CMS\Core\Type\ContextualFeedbackSeverity::NOTICE);
            };

            GeneralUtility::makeInstance(OpcodeCacheService::class)->clearAllActive();
        }
        if($this->messageMethod == self::MESSAGE_RETURN_DATA) {
            return $this->messages;
        } else {
            return null;
        }
    }


    /**
     * @param $configuration
     * @param $lastSync_fileModificationTime
     * @param $identifierField
     */
    protected function updateBackendGroups($configuration, $lastSync_fileModificationTime, $identifierField) {
        $updateSuccess = true;
        /** @var QueryBuilder $queryBuilder */
        $queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)->getQueryBuilderForTable('be_groups');

        if(count($configuration) > 0) {
            $this->addMessage(
                Utility::getLanguageService()->sL('LLL:EXT:access_control_sync/Resources/Private/Language/locallang_db.xlf:flashMessage.begroupsUpdateInfoMessage'),
                'LLL:EXT:access_control_sync/Resources/Private/Language/locallang_db.xlf:flashMessage.begroupsUpdateInfoTitle',
                \TYPO3\CMS\Core\Type\ContextualFeedbackSeverity::INFO);
            if($identifierField !== 'tx_access_control_sync_uuid') {
                $this->addMessage(
                    'IdentifierField:' . $identifierField,
                    Utility::getLanguageService()->sL('LLL:EXT:access_control_sync/Resources/Private/Language/locallang_db.xlf:flashMessage.alternativeIdentifierFieldMessage'),
                    \TYPO3\CMS\Core\Type\ContextualFeedbackSeverity::NOTICE);
            }
        }

        if(Utility::getExtensionConfiguration('autoCreateMissingRecords')) {
            $this->createMissingRecords($configuration, $identifierField, $queryBuilder);
        }

        // Detect duplicate identifier usages
        $qb = clone $queryBuilder;
        $qb->select($identifierField)->select($identifierField)
            ->addSelectLiteral($qb->expr()->count($identifierField, 'cnt'))
            ->from('be_groups')->where(
            $qb->expr()->in($identifierField, $qb->createNamedParameter(array_keys($configuration), Connection::PARAM_INT_ARRAY))
        )->groupBy($identifierField);
        $duplicateIdentifiers = array_filter($qb->execute()->fetchAllAssociative(), function ($v, $k) use ($identifierField) {
            if ($v[$identifierField] == '') {
                return false;
            }
            return $v['cnt'] !== 1;
        }, ARRAY_FILTER_USE_BOTH);

        if($duplicateIdentifiers !== []) {
            $updateSuccess = false;
            foreach ($duplicateIdentifiers as $duplicateIdentifier) {
                $this->addMessage('Multiple records use the same identifier, which should not happen!','Duplicate identifiers detected', \TYPO3\CMS\Core\Type\ContextualFeedbackSeverity::ERROR, $duplicateIdentifier[$identifierField]);
            }
        } else {
            $fields = array_keys(\TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Database\ConnectionPool::class)->getConnectionForTable('be_groups')->createSchemaManager()->listTableColumns('be_groups'));
            foreach ($configuration as $row) {
                $row = array_intersect_key($row, array_flip($fields));
                // Only update
                // If identifier is not empty (else all records with empty identifier will be overwritten!)
                // And if changes for this record since last sync
                if(($row[$identifierField] ?? false) && $row['tstamp'] > $lastSync_fileModificationTime) {
                    $updateData = array_diff_key($row, array_flip(static::$fieldNameUpdateFilter), Utility::getExcludeFields());
                    $updateData['tx_access_control_sync_tstamp'] = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Context\Context::class)->getPropertyFromAspect('date', 'timestamp');

                    // Find uids for subgroup field
                    if($row['subgroupFromUuid'] ?? false) {
                        /** @var QueryBuilder $queryBuilder */
                        $queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)->getQueryBuilderForTable('be_groups');
                        $result = $queryBuilder->from('be_groups')->select('uid')->where($queryBuilder->expr()->in(
                            'tx_access_control_sync_uuid',
                            $queryBuilder->createNamedParameter($row['subgroupFromUuid'], Connection::PARAM_STR_ARRAY)
                        ))->executeQuery()->fetchAllAssociative();
                        $updateData['subgroup'] = implode(",", array_column($result, 'uid'));
                    }

                    /** @var Connection $connection */
                    $connection = GeneralUtility::makeInstance(ConnectionPool::class)->getConnectionForTable('be_groups');
                    $result = $connection->update('be_groups', $updateData, [$identifierField => $row[$identifierField], 'deleted' => 0]);
                    $message = '';
                    if(!$result) {
                        $updateSuccess = false;
                        if($identifierField != 'tx_access_control_sync_uuid') {
                            $message = Utility::getExtensionConfiguration()['identifierField'] . Utility::getLanguageService()->sL('LLL:EXT:access_control_sync/Resources/Private/Language/locallang_db.xlf:flashMessage.begroupsUpdateFailIdentifierOtherMessage') . ' ' . $row[Utility::getExtensionConfiguration()['identifierField']];
                        } else {
                            $message = Utility::getLanguageService()->sL('LLL:EXT:access_control_sync/Resources/Private/Language/locallang_db.xlf:flashMessage.begroupsUpdateFailIdentifierUidMessage') . ' ' . $row['tx_access_control_sync_uuid'];
                        }
                        $title = Utility::getLanguageService()->sL('LLL:EXT:access_control_sync/Resources/Private/Language/locallang_db.xlf:flashMessage.begroupsUpdateFailTitle');
                    } else {
                        $title = Utility::getLanguageService()->sL('LLL:EXT:access_control_sync/Resources/Private/Language/locallang_db.xlf:flashMessage.begroupsUpdateSuccessTitle');
                    }
                    $title .= " {$row['title']}";

                    $this->addMessage($message, $title,
                        ($result ? \TYPO3\CMS\Core\Type\ContextualFeedbackSeverity::OK : \TYPO3\CMS\Core\Type\ContextualFeedbackSeverity::WARNING), $row['tx_access_control_sync_uuid']);
                }
            }
        }
        return $updateSuccess;
    }

    /**
     * @param $updateData
     * @param $identifierField
     * @param $subgroupFromIdentifierField
     * @param $queryBuilder
     */
    protected function getSubgroupsFromIdentifierField(&$updateData, $identifierField, $subgroupFromIdentifierField, $queryBuilder) {
        if($identifierField !== 'uid') {
            $result = $queryBuilder->from('be_groups')->select('uid')->where(
                $queryBuilder->expr()->in(
                    Utility::getExtensionConfiguration('identifierField'),
                    $queryBuilder->createNamedParameter($subgroupFromIdentifierField, Connection::PARAM_STR_ARRAY)
                )
            )->execute()->fetchAll();
            $updateData['subgroup'] = implode(",", array_column($result, 'uid'));
        }
    }

    /**
     * @param $configuration
     * @param $identifierField
     * @param $queryBuilder
     */
    protected function createMissingRecords($configuration, $identifierField, $queryBuilder) {
        if(Environment::isCli()) {
            // Automatically creates and initializes _cli_ user
            Bootstrap::initializeBackendAuthentication();
        }
        // Only auto create fields if identifierField is tx_access_control_sync_uuid
        if ($identifierField !== 'tx_access_control_sync_uuid') {
            return;
        }
        // Create missing be_groups records
        $identifiersArray = array_column($configuration, $identifierField);
        //find which records are not created yet, from records which already exists
        $allExistingRecords = $queryBuilder->from('be_groups')->select($identifierField)->where(
            $queryBuilder->expr()->in($identifierField, $queryBuilder->createNamedParameter($identifiersArray, Connection::PARAM_STR_ARRAY))
        )->execute()->fetchAll();

        /** @var DataHandler $dataHandler */
        $dataHandler = GeneralUtility::makeInstance(DataHandler::class);
        $dataHandler->BE_USER = $GLOBALS['BE_USER'];
        foreach(array_unique(array_diff($identifiersArray, array_column($allExistingRecords, $identifierField))) as $value) {
            // Do not create records marked as deleted
            if(($configuration[$value] ?? false) && !$configuration[$value]['deleted']) {
                $dataHandler->start(['be_groups'=>['NEW_AUTOCREATE' => ['pid' => 0, $identifierField=> $value]]], []);
                $dataHandler->process_datamap();
                $this->addMessage("Missing be_groups record created:\nTitle: " . ($configuration[$value]['title'] ."\nUid: " . $configuration[$value]['uid']), 'NOTICE!',
                    \TYPO3\CMS\Core\Type\ContextualFeedbackSeverity::NOTICE, $value);
            }
        }
    }
}
