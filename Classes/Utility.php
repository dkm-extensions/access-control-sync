<?php


namespace DKM\AccessControlSync;


use TYPO3\CMS\Core\Configuration\ConfigurationManager;
use TYPO3\CMS\Core\Configuration\Exception\ExtensionConfigurationExtensionNotConfiguredException;
use TYPO3\CMS\Core\Configuration\Exception\ExtensionConfigurationPathDoesNotExistException;
use TYPO3\CMS\Core\Configuration\ExtensionConfiguration;
use TYPO3\CMS\Core\Core\Bootstrap;
use TYPO3\CMS\Core\Core\Environment;
use TYPO3\CMS\Core\Exception;
use TYPO3\CMS\Core\Messaging\FlashMessage;
use TYPO3\CMS\Core\Messaging\FlashMessageService;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Lang\LanguageService;

class Utility
{
    /**
     * @param null $key
     * @return false|mixed
     * @throws ExtensionConfigurationExtensionNotConfiguredException
     * @throws ExtensionConfigurationPathDoesNotExistException
     */
    public static function getExtensionConfiguration($key = null): mixed
    {
        static $configuration;
        $configuration = $configuration ?? GeneralUtility::makeInstance(ExtensionConfiguration::class)->get('access_control_sync');
        if ($key) {
            return $configuration[$key] ?? false;
        } else {
            return $configuration;
        }
    }

    /**
     * @throws Exception
     */
    public static function getSyncFilePath()
    {
        $syncFilePath = self::getExtensionConfiguration()['syncFilePath'] ?? false;
        if (!$syncFilePath) {
            throw new Exception('EXT:access_control_sync - syncFilePath not defined');
        }
        // build the file path relative to the public web path
        if (strpos($syncFilePath, 'EXT:') === 0) {
            $syncFilePath = GeneralUtility::getFileAbsFileName($syncFilePath);
        } elseif (strpos($syncFilePath, '../') === 0) {
            $syncFilePath = GeneralUtility::resolveBackPath(Environment::getBackendPath() . '/../' . $syncFilePath);
        } else {
            $syncFilePath = Environment::getPublicPath() . '/' . $syncFilePath;
        }
        if (!$syncFilePath) {
            throw new Exception('No valid syncFilePath configured for EXT:access_control_sync');
        }
        return $syncFilePath;
    }

    public static function setLastSync_fileModificationTime($value)
    {
        /** @var ConfigurationManager $configurationManager */
        $configurationManager = GeneralUtility::makeInstance(ConfigurationManager::class);
        $content = file_get_contents($configurationManager->getAdditionalConfigurationFileLocation());
        $lines = preg_split("/\r\n|\n|\r/", $content);
        // remove first line <?php as it will be added again later
        unset($lines[0]);

        //Find if configuration was already set
        $variableName = '$GLOBALS[\'TYPO3_CONF_VARS\'][\'EXTENSIONS\'][\'access_control_sync\'][\'lastSync_fileModificationTime\']';
        $result = array_filter($lines, function ($item) use ($variableName) {
            return strpos($item, $variableName) !== false;
        });

        if ($result !== []) {
            $lines[key($result)] = $variableName . ' = ' . $value . ';';
        } else {
            $lines[] = '// IMPORTANT: This line has been added automatically by EXT:access_control_sync, and will be updated automatically as well!';
            $lines[] = $variableName . ' = ' . $value . ';';
        }
        // Write new configuration
        $configurationManager->writeAdditionalConfiguration($lines);
    }

    /**
     * @return mixed|string
     */
    public static function getMode()
    {
        $extCfg = GeneralUtility::makeInstance(ExtensionConfiguration::class)->get('access_control_sync');
        $extCfg['mode'] = $extCfg['mode'] ?? 'primary';
        if ($extCfg['mode'] === 'Production/Live' || $extCfg['mode'] === 'Production/Staging') {
            return strpos(self::getContext(), $extCfg['mode']) === 0 ? 'primary' : 'replica';
        }
        return $extCfg['mode'];
    }

    /**
     * @return mixed|string
     */
    public static function getContext()
    {
        if (Environment::isCli()) {
            return $_ENV['TYPO3_CONTEXT'];
        }
        return (string)Environment::getContext();
    }

    /**
     * @return array
     */
    public static function getExcludeFields()
    {
        $excludeFields = GeneralUtility::trimExplode(',', self::getExtensionConfiguration()['excludeFields'] ?? '');
        $excludeFields[] = 'subgroupFromUuid';
        $excludeFields[] = 'subgroup';
        return array_flip($excludeFields);
    }

    /**
     * @param $timeStamp
     * @return string
     * @throws Exception
     */
    public static function getNewFileName($timeStamp)
    {
        $syncFilePath = self::getSyncFilePath();
        $arr = explode('.', $syncFilePath);
        array_splice($arr, -1, 0, $timeStamp);
        return implode('.', $arr);
    }

    /**
     * @return mixed
     * @throws Exception
     */
    public static function getSyncStoredFileName()
    {
        static $storedFileName;
        if ($storedFileName) {
            return $storedFileName;
        }
        $syncFilePath = self::getSyncFilePath();

        $files = glob(substr($syncFilePath, 0, strrpos($syncFilePath, '.')) . '*');
        if ($files) {
            natsort($files);
            $storedFileName = array_pop($files);
            return $storedFileName;
        }
        return false;
    }

    /**
     * @param $fileName
     * @return false|int
     */
    public static function getTimeFromSyncFileName($fileName)
    {
        $value = (int)current(array_slice(explode('.', $fileName), -2, 1));
        return $value !== 0 ? $value : false;
    }

    /**
     * @param $message
     * @param $title
     * @param int $severity
     */
    public static function flashMessage($message, $title, $severity = \TYPO3\CMS\Core\Type\ContextualFeedbackSeverity::NOTICE)
    {
        $flashMessageService = GeneralUtility::makeInstance(FlashMessageService::class);
        $flashMessageQueue = $flashMessageService->getMessageQueueByIdentifier();

        $message = GeneralUtility::makeInstance(FlashMessage::class,
            self::getLanguageService()->sL($message),
            self::getLanguageService()->sL($title),
            $severity,
            true
        );
        $flashMessageQueue->addMessage($message);
    }

    /**
     * @return LanguageService
     */
    public static function getLanguageService()
    {
        if (!$GLOBALS['LANG']) {
            Bootstrap::initializeLanguageObject();
        }
        return $GLOBALS['LANG'];
    }

    /**
     * @param $int
     * @return string
     */
    static public function getSeverity($int)
    {
        switch ($int) {
            case \TYPO3\CMS\Core\Type\ContextualFeedbackSeverity::NOTICE:
                return 'NOTICE';
            case \TYPO3\CMS\Core\Type\ContextualFeedbackSeverity::WARNING:
                return 'WARNING';
            case \TYPO3\CMS\Core\Type\ContextualFeedbackSeverity::ERROR:
                return 'ERROR';
            case \TYPO3\CMS\Core\Type\ContextualFeedbackSeverity::INFO:
                return 'INFO';
            case \TYPO3\CMS\Core\Type\ContextualFeedbackSeverity::OK:
                return 'OK';
            default:
                return '???';
        }
    }


}