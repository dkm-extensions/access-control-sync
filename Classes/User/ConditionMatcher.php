<?php
namespace DKM\AccessControlSync\User;

use DKM\AccessControlSync\Utility;

class ConditionMatcher
{
    /**
     * @return bool
     */
    public function checkModeIsPrimary() {
        return Utility::getMode() == 'primary';
    }
}