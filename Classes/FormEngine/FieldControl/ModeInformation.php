<?php
declare(strict_types=1);

namespace DKM\AccessControlSync\FormEngine\FieldControl;

use DKM\AccessControlSync\Utility;
use Symfony\Component\Console\Helper\FormatterHelper;
use TYPO3\CMS\Backend\Form\AbstractNode;
use TYPO3\CMS\Core\Messaging\FlashMessage;

class ModeInformation extends AbstractNode
{
    public function render()
    {
        if(Utility::getMode() == 'replica' && ($this->data['databaseRow']['tx_access_control_sync_activated'] ?? false)) {
            Utility::flashMessage(
                'LLL:EXT:access_control_sync/Resources/Private/Language/locallang_db.xlf:flashMessage.replicaModeWarningMessage',
                'LLL:EXT:access_control_sync/Resources/Private/Language/locallang_db.xlf:flashMessage.replicaModeWarningTitle',
                FlashMessage::WARNING);
            Utility::flashMessage(
                Utility::getLanguageService()->sL(FormatterHelper::formatTime($GLOBALS['EXEC_TIME'] - $this->data['databaseRow']['tx_access_control_sync_tstamp'])) ,
                'LLL:EXT:access_control_sync/Resources/Private/Language/locallang_db.xlf:flashMessage.replicaModeTimeInfo',
                FlashMessage::INFO);
        }
        return ['html'=>''];
    }
}