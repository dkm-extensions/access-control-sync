<?php
namespace DKM\AccessControlSync\Hooks;

use DKM\AccessControlSync\Service\DatabaseUpdateService;
use DKM\AccessControlSync\Utility;
use TYPO3\CMS\Core\Utility\GeneralUtility;

class UpdateDatabase
{
    public function process($params, $backendUserAuthentication) {
        $extCfg = Utility::getExtensionConfiguration();
        if($backendUserAuthentication->isAdmin() && ($extCfg['syncFilePath'] ?? false) && Utility::getMode() == 'replica') {
            /** @var DatabaseUpdateService $databaseUpdateService */
            $databaseUpdateService = GeneralUtility::makeInstance(DatabaseUpdateService::class);
            $databaseUpdateService->setMessageMethod(DatabaseUpdateService::MESSAGE_FLASH);
            $databaseUpdateService->process();
        }
    }
}