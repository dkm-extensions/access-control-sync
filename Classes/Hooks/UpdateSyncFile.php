<?php
namespace DKM\AccessControlSync\Hooks;

use DKM\AccessControlSync\Utility;
use TYPO3\CMS\Backend\Utility\BackendUtility;
use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Database\Query\QueryBuilder;
use TYPO3\CMS\Core\DataHandling\DataHandler;
use TYPO3\CMS\Core\SingletonInterface;
use TYPO3\CMS\Core\Utility\GeneralUtility;

class UpdateSyncFile implements SingletonInterface
{
    protected $syncData;

    /**
     * @param $obj
     * @param DataHandler $dataHandler
     */
    public function processDatamap_afterAllOperations($dataHandler) {
        if($this->syncData !== null && Utility::getMode() == 'primary') {
            $newFileName = Utility::getNewFileName(\TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Context\Context::class)->getPropertyFromAspect('date', 'timestamp'));
            file_put_contents($newFileName, "<?php \n return " . var_export($this->syncData, true) . ';');
            $storedFileName = Utility::getSyncStoredFileName();
            if ($storedFileName != $newFileName && file_exists($storedFileName)) {
                unlink($storedFileName);
            }
        }
    }

    /**
     * @param $status
     * @param $table
     * @param $id
     * @param $fieldArray
     * @param $dataHandler
     * @throws \TYPO3\CMS\Core\Exception
     */
    public function processDatamap_afterDatabaseOperations($status, $table, $id, $fieldArray, $dataHandler) {
        if ($table === 'be_groups' && Utility::getMode() == 'primary') {
            // Only sync if sync is activated
            $uuid = $fieldArray['tx_access_control_sync_uuid'] ?? ($dataHandler->checkValue_currentRecord['tx_access_control_sync_uuid'] ?? false);
            if($uuid && (isset($fieldArray['tx_access_control_sync_activated']) || ($dataHandler->checkValue_currentRecord['tx_access_control_sync_activated'] ?? false))) {
                $this->updateSyncFileData(is_int($id) ? $id : $dataHandler->substNEWwithIDs[$id], $fieldArray, $uuid, $dataHandler);
            }
        }
    }

    /**
     * @param $command
     * @param $table
     * @param $id
     * @param $value
     * @param $dataHandler
     * @param $pasteUpdate
     * @param $pasteDatamap
     * @throws \TYPO3\CMS\Core\Exception
     */
    public function processCmdmap_postProcess($command, $table, $id, $value, $dataHandler, $pasteUpdate, $pasteDatamap) {
        if($dataHandler->cmdmap['be_groups'] ?? false) {
            $recordsToRemove = [];
            foreach ($dataHandler->cmdmap['be_groups'] as $uid => $command) {
                if(key($command) == 'delete') {
                    $recordsToRemove[] = $uid;
                }
            }
            if($recordsToRemove !== []) {
                $this->markDeletedRecordsFromData($recordsToRemove);
            }
        }
    }

    /**
     * @param $status
     * @param $table
     * @param $id
     * @param $fieldArray
     * @param $dataHandler
     */
    public function processDatamap_postProcessFieldArray($status, $table, $id, &$fieldArray, $dataHandler) {
        if($table === 'be_groups') {
            //reset 'tx_access_control_sync_activated' and 'tx_access_control_sync_uuid' when record is a copy
            if(str_starts_with($id, 'NEW') && !str_starts_with($id, 'NEW_AUTOCREATE') &&
                ($dataHandler->datamap['be_groups'][$id] ?? false) &&
                ($dataHandler->datamap['be_groups'][$id]['tx_access_control_sync_uuid'] ?? false)) {
                $fieldArray['tx_access_control_sync_uuid'] = '';
                $fieldArray['tx_access_control_sync_activated'] = '';
            }
            //Automatically generate a new uuid sync is activated
            if(($dataHandler->checkValue_currentRecord['tx_access_control_sync_activated'] ?? false) &&
                !($dataHandler->checkValue_currentRecord['tx_access_control_sync_uuid'] ?? false)) {
                $fieldArray['tx_access_control_sync_uuid'] = (string)\Ramsey\Uuid\Nonstandard\Uuid::uuid4();
            }
        }
    }

    /**
     * @param $uid
     * @param $fieldArray
     * @param $uuid
     * @param DataHandler $dataHandler
     * @throws \TYPO3\CMS\Core\Exception
     */
    protected function updateSyncFileData($uid, $fieldArray, $uuid, $dataHandler) {
        $this->syncData = $this->getFileData();
        // Seems this will ensure that the command will not be executed multiple times?
        $this->syncData[$uuid] = array_merge($this->getDataForRecord($this->syncData, $uuid, $uid), $fieldArray ?? []);
        if($this->syncData[$uuid]['subgroup']) {
            /** @var QueryBuilder $queryBuilder */
            $queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)->getQueryBuilderForTable('be_groups');
            $queryBuilder->from('be_groups')->add('where','uid IN (' . $this->syncData[$uuid]['subgroup'] .')')->select('tx_access_control_sync_uuid');
            $this->syncData[$uuid]['subgroupFromUuid'] = array_filter(array_column($queryBuilder->execute()->fetchAll(), 'tx_access_control_sync_uuid'));
        }
        // We need to update the tstamp field manually, as the new value is not provided by historyRecords
        $this->syncData[$uuid]['tstamp'] = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Context\Context::class)->getPropertyFromAspect('date', 'timestamp');
    }

    /**
     * @throws \TYPO3\CMS\Core\Exception
     */
    protected function markDeletedRecordsFromData(array $uids) {
        $this->getFileData();
    }

    /**
     * @return array|mixed
     * @throws \TYPO3\CMS\Core\Exception
     */
    protected function getFileData() {
        if ($this->syncData) {
            return $this->syncData;
        }
        $fileName = Utility::getSyncStoredFileName();
        $this->syncData = file_exists($fileName) ? require $fileName : [];
        return $this->syncData;
    }

    /**
     * @param $dataFromFile
     * @param $uuid
     * @param $uid
     * @return array|mixed|null
     */
    protected function getDataForRecord($dataFromFile, $uuid, $uid) {
        if($dataFromFile[$uuid] ?? false) {
            return $dataFromFile[$uuid];
        } else {
            return BackendUtility::getRecord('be_groups',$uid);
        }
    }
}
