CREATE TABLE be_groups (
      tx_access_control_sync_activated tinyint(4) DEFAULT '0' NOT NULL,
      tx_access_control_sync_tstamp int(11) DEFAULT '0' NOT NULL,
      tx_access_control_sync_uuid char(36) DEFAULT '' NOT NULL
);