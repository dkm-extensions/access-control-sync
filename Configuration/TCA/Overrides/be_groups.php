<?php
defined('TYPO3') || die();
$temporaryColumns = [
    'tx_access_control_sync_activated' => [
        'exclude' => 0,
        'label' => 'LLL:EXT:access_control_sync/Resources/Private/Language/locallang_db.xlf:be_groups.tx_access_control_sync_activated',
        'config' => [
            'type' => 'check',
            'size' => 1,
            'maxitems' => 1,
            'readOnly' => \DKM\AccessControlSync\Utility::getMode() != 'primary'
        ],
//        'displayCond' => 'USER:DKM\\AccessControlSync\\User\\ConditionMatcher->checkModeIsPrimary'
    ],
    'tx_access_control_sync_tstamp' => [
        'exclude' => 0,
        'label' => 'LLL:EXT:access_control_sync/Resources/Private/Language/locallang_db.xlf:be_groups.tx_access_control_sync_tstamp',
        'config' => [
            'type' => 'passthrough'
        ]
    ],
    'tx_access_control_sync_uuid' => [
        'exclude' => 0,
        'label' => 'LLL:EXT:access_control_sync/Resources/Private/Language/locallang_db.xlf:be_groups.tx_access_control_sync_uuid',
        'config' => [
            'type' => 'input',
            'readOnly' => 1
        ],
        'displayCond' => 'FIELD:tx_access_control_sync_uuid:REQ:true'
    ]

];
$GLOBALS['TCA']['be_groups']['columns']['title']['config']['fieldInformation']['modeInformationKey']['renderType'] = 'modeInformation';

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns(
    'be_groups',
    $temporaryColumns
);
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addFieldsToPalette('be_groups','accesscontrolsync','tx_access_control_sync_activated,tx_access_control_sync_uuid');
// Insert first
$showItemExploded = \TYPO3\CMS\Core\Utility\GeneralUtility::trimExplode(',', $GLOBALS['TCA']['be_groups']['types'][0]['showitem']);
array_splice($showItemExploded,1,0, '--palette--;;accesscontrolsync');
$GLOBALS['TCA']['be_groups']['types'][0]['showitem'] =  implode(',', $showItemExploded);
