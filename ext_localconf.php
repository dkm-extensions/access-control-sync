<?php

if (!defined('TYPO3')) {
    die ('Access denied.');
}
if($GLOBALS['TYPO3_CONF_VARS']['EXTENSIONS']['access_control_sync']['enableDBUpdateOnAdminLogin'] ?? false) {
    $GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['t3lib/class.t3lib_userauthgroup.php']['backendUserLogin']['access-control-sync'] = \DKM\AccessControlSync\Hooks\UpdateDatabase::class . '->process';
}

$GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['t3lib/class.t3lib_tcemain.php']['processDatamapClass']['access-control-sync'] = \DKM\AccessControlSync\Hooks\UpdateSyncFile::class;

$GLOBALS['TYPO3_CONF_VARS']['SYS']['formEngine']['nodeRegistry'][12485351217] = [
    'nodeName' => 'modeInformation',
    'priority' => 30,
    'class' => \DKM\AccessControlSync\FormEngine\FieldControl\ModeInformation::class
];